+++

+++

<div id="main">

<div id="page-section">
<h1>Site</h1>
<p>This site was built using <a href="https://gohugo.io">Hugo</a> and is hosted on <a href="https://about.gitlab.com">GitLab</a>. Layout and construction were heavily influenced by Pixyll.</p>
</div>

</div>
